package tpSpringMvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import tpSpringMvc.models.Address;
import tpSpringMvc.models.Event;
import tpSpringMvc.service.AddressService;
import tpSpringMvc.service.EventService;

@Controller
@RequestMapping(value="/events")
public class EventController {
	@Autowired EventService eventServ;
	@Autowired AddressService addrServ;
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView list () {
		ModelAndView model = new ModelAndView("events/event_page"); //Lie a une page (constr, lien Vue), et va permettre lien d'objets (Model) 
		List<Event> listEvent = this.eventServ.findAll();
		
		model.addObject("listEvent", listEvent); //Variable utilisable dans la page JSP (Vue) sous le nom listEmployee
		
		return model;
	}
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public ModelAndView add () {
		ModelAndView model = new ModelAndView("events/event_form");
		Event ev = new Event();
		Address addr = new Address();
		
		model.addObject("eventForm", ev);
		model.addObject("addressForm", addr);
		model.addObject("addrList", this.addrServ.findAll());
		
		return model;
	}
	
	@RequestMapping(value="/update/{id}", method = RequestMethod.GET)
	public ModelAndView update (@PathVariable("id") int id) {
		ModelAndView model = new ModelAndView("events/event_form");
		Event ev = this.eventServ.findById(id);
		Address addr = new Address();

		model.addObject("eventForm", ev);
		model.addObject("addressForm", addr);
		model.addObject("addrList", this.addrServ.findAll());

		return model;
	}
	
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public ModelAndView save (@ModelAttribute("eventForm") Event ev, @ModelAttribute("addressForm") Address addr) {
		this.eventServ.createOrUpdateEvent(ev);
		this.addrServ.createOrUpdateAddress(addr);
		
		//TODO lier ev et addr
		
		return new ModelAndView("redirect:/events/list");
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public ModelAndView delete (@PathVariable("id") int id) {
		Event ev = this.eventServ.findById(id);
		//this.addrServ.deleteAddress(ev.addr);
		this.eventServ.deleteEvent(ev);
		return new ModelAndView("redirect:/events/list");
	}
}
