package tpSpringMvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import tpSpringMvc.models.Item;
import tpSpringMvc.service.ItemService;

@Controller
@RequestMapping(value="/items")
public class ItemController {
	@Autowired ItemService itemServ;
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView list () {
		ModelAndView model = new ModelAndView("items/item_page"); //Lie a une page (constr, lien Vue), et va permettre lien d'objets (Model) 
		List<Item> listItem = this.itemServ.findAll();
		
		model.addObject("listItem", listItem); //Variable utilisable dans la page JSP (Vue) sous le nom listEmployee
		
		return model;
	}
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public ModelAndView add () {
		ModelAndView model = new ModelAndView("items/item_form");
		Item item = new Item();
		
		model.addObject("itemForm", item);
		
		return model;
	}
	
	@RequestMapping(value="/update/{id}", method = RequestMethod.GET)
	public ModelAndView update (@PathVariable("id") int id) {
		ModelAndView model = new ModelAndView("items/item_form");
		Item item = this.itemServ.findById(id);
		
		model.addObject("itemForm", item);
		
		return model;
	}
	
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public ModelAndView save (@ModelAttribute("guestForm") Item i) {
		this.itemServ.createOrUpdateItem(i);
		
		return new ModelAndView("redirect:/items/list");
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public ModelAndView delete (@PathVariable("id") int id) {
		this.itemServ.deleteItem(this.itemServ.findById(id));
		return new ModelAndView("redirect:/items/list");
	}
}
