package tpSpringMvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import tpSpringMvc.models.Guest;
import tpSpringMvc.service.GuestService;

@Controller
@RequestMapping(value="/guests")
public class GuestController {
	@Autowired GuestService guestServ;
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView list () {
		ModelAndView model = new ModelAndView("guests/guest_page"); //Lie a une page (constr, lien Vue), et va permettre lien d'objets (Model) 
		List<Guest> listGuest = this.guestServ.findAll();
		
		model.addObject("listGuest", listGuest); //Variable utilisable dans la page JSP (Vue) sous le nom listEmployee
		
		return model;
	}
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public ModelAndView add () {
		ModelAndView model = new ModelAndView("guests/guest_form");
		Guest guest = new Guest();
		
		model.addObject("guestForm", guest);
		
		return model;
	}
	
	@RequestMapping(value="/update/{id}", method = RequestMethod.GET)
	public ModelAndView update (@PathVariable("id") int id) {
		ModelAndView model = new ModelAndView("guests/guest_form");
		Guest guest = this.guestServ.findById(id);
		
		model.addObject("guestForm", guest);
		
		return model;
	}
	
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public ModelAndView save (@ModelAttribute("guestForm") Guest g) {
		this.guestServ.createOrUpdateGuest(g);
		
		return new ModelAndView("redirect:/guests/list");
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public ModelAndView delete (@PathVariable("id") int id) {
		this.guestServ.deleteGuest(this.guestServ.findById(id));
		return new ModelAndView("redirect:/guests/list");
	}
}
