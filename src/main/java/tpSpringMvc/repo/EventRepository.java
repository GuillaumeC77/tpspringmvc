package tpSpringMvc.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import tpSpringMvc.models.Event;


public interface EventRepository extends JpaRepository<Event, Integer> {
	public List<Event> findAll ();
	public Page<Event> findAll (Pageable p);
	public Event findById (int id);
	public List<Event> findByTitle (String t);
	public Page<Event> findByTitle (String t, Pageable p);
}
