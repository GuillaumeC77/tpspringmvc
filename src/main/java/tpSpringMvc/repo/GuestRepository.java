package tpSpringMvc.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import tpSpringMvc.models.Guest;

public interface GuestRepository extends JpaRepository<Guest, Integer> {
	public List<Guest> findAll ();
	public Page<Guest> findAll (Pageable p);
	public Guest findById (int id);
	public List<Guest> findByName (String n);
	public Page<Guest> findByName (String n, Pageable p);
	public List<Guest> findByEmail (String e);
	//public Page<Guest> findByEmail (String e, Pageable p);
}
