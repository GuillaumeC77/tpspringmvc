package tpSpringMvc.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import tpSpringMvc.models.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	public List<User> findAll ();
	public Page<User> findAll (Pageable p);
	public User findById (int id);
	public List<User> findByEmail (String e);
	public User findByLoginAndPass (String l, String p);
}
