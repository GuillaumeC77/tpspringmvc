package tpSpringMvc.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import tpSpringMvc.models.Item;

public interface ItemRepository extends JpaRepository<Item, Integer> {
	public List<Item> findAll ();
	public Page<Item> findAll (Pageable p);
	public Item findById (int id);
	public List<Item> findByName (String n);
	public Page<Item> findByName (String n, Pageable p);
}
