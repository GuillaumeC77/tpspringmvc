package tpSpringMvc.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import tpSpringMvc.models.Address;


public interface AddressRepository extends JpaRepository<Address, Integer> {
	//nom, rue, ZipCode, ville
	
	public List<Address> findAll ();
	public Page<Address> findAll (Pageable p);
	public Address findById (int id);
	public List<Address> findByCity (String c);
	public Page<Address> findByCity (String c, Pageable p);
	
}
