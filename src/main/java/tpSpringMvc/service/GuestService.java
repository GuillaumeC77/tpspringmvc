package tpSpringMvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import tpSpringMvc.models.Guest;
import tpSpringMvc.repo.GuestRepository;

@Service
public class GuestService implements IGuestService {

	@Autowired
	private GuestRepository guestRepo;
	
	public void createOrUpdateGuest (Guest g) {
		this.guestRepo.save(g);
	}
	
	public void deleteGuest (Guest g) {
		this.guestRepo.delete(g);
	}
	
	public List<Guest> findAll() {
		return this.guestRepo.findAll();
	}

	public Page<Guest> findAll(Pageable p) {
		return this.guestRepo.findAll(p);
	}

	public Guest findById(int id) {
		return this.guestRepo.findById(id);
	}

	public List<Guest> findByName(String n) {
		return this.guestRepo.findByName(n);
	}

	public Page<Guest> findByName(String n, Pageable p) {
		return this.guestRepo.findByName(n, p);
	}

	public List<Guest> findByEmail(String e) {
		return this.guestRepo.findByEmail(e);
	}

}
