package tpSpringMvc.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import tpSpringMvc.models.Guest;


public interface IGuestService {
	public List<Guest> findAll ();
	public Page<Guest> findAll (Pageable p);
	public Guest findById (int id);
	public List<Guest> findByName (String n);
	public Page<Guest> findByName (String n, Pageable p);
	public List<Guest> findByEmail (String e);
	//public Page<Guest> findByEmail (String e, Pageable p);
}
