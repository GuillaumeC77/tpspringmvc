package tpSpringMvc.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import tpSpringMvc.models.Address;


public interface IAddressService {
	public List<Address> findAll ();
	public Page<Address> findAll (Pageable p);
	public Address findById (int id);
	public List<Address> findByCity (String c);
	public Page<Address> findByCity (String c, Pageable p);
}
