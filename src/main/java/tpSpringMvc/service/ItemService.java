package tpSpringMvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import tpSpringMvc.models.Item;
import tpSpringMvc.repo.ItemRepository;

@Service
public class ItemService implements IItemService {
	@Autowired
	private ItemRepository itemRepo;
	
	public void createOrUpdateItem (Item i) {
		this.itemRepo.save(i);
	}
	
	public void deleteItem (Item i) {
		this.itemRepo.delete(i);
	}

	public List<Item> findAll() {
		return this.itemRepo.findAll();
	}

	public Page<Item> findAll(Pageable p) {
		return this.itemRepo.findAll(p);
	}

	public Item findById(int id) {
		return this.itemRepo.findById(id);
	}

	public List<Item> findByName(String n) {
		return this.itemRepo.findByName(n);
	}

	public Page<Item> findByName(String n, Pageable p) {
		return this.itemRepo.findByName(n, p);
	}

}
