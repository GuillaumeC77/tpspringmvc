package tpSpringMvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import tpSpringMvc.models.Event;
import tpSpringMvc.repo.EventRepository;


@Service
public class EventService implements IEventService {
	
	@Autowired
	private EventRepository eventRepo;
	
	public void createOrUpdateEvent (Event e) {
		this.eventRepo.save(e);
	}
	
	public void deleteEvent (Event e) {
		this.eventRepo.delete(e);
	}

	public List<Event> findAll() {
		return this.eventRepo.findAll();
	}

	public Page<Event> findAll(Pageable p) {
		return this.eventRepo.findAll(p);
	}

	public Event findById(int id) {
		return this.eventRepo.findById(id);
	}

	public List<Event> findByTitle(String t) {
		return this.eventRepo.findByTitle(t);
	}

	public Page<Event> findByTitle(String t, Pageable p) {
		return this.eventRepo.findByTitle(t, p);
	}

}
