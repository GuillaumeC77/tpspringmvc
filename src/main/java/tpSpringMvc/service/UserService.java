package tpSpringMvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import tpSpringMvc.models.User;
import tpSpringMvc.repo.UserRepository;

@Service
public class UserService implements IUserService {
	@Autowired
	private UserRepository userRepo;
	
	public void createOrUpdateUser (User u) {
		this.userRepo.save(u);
	}
	
	public void deleteUser (User u) {
		this.userRepo.delete(u);
	}

	public List<User> findAll() {
		return this.userRepo.findAll();
	}

	public Page<User> findAll(Pageable p) {
		return this.userRepo.findAll(p);
	}

	public User findById(int id) {
		return this.userRepo.findById(id);
	}

	public List<User> findByEmail(String e) {
		return this.userRepo.findByEmail(e);
	}

	public User findByLoginAndPass(String l, String p) {
		return this.userRepo.findByLoginAndPass(l, p);
	}

}
