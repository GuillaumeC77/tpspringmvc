package tpSpringMvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import tpSpringMvc.models.Address;
import tpSpringMvc.repo.AddressRepository;


@Service
public class AddressService implements IAddressService {
	
	@Autowired
	private AddressRepository addressRepo;
	
	public void createOrUpdateAddress (Address addr) {
		this.addressRepo.save(addr);
	}
	
	public void deleteAddress (Address addr) {
		this.addressRepo.delete(addr);
	}

	public List<Address> findAll() {
		return this.addressRepo.findAll();
	}

	public Page<Address> findAll(Pageable p) {
		return this.addressRepo.findAll(p);
	}

	public Address findById(int id) {
		return this.addressRepo.findById(id);
	}

	@Override
	public List<Address> findByCity(String c) {
		return this.addressRepo.findByCity(c);
	}

	@Override
	public Page<Address> findByCity(String c, Pageable p) {
		return this.addressRepo.findByCity(c, p);
	}

}
