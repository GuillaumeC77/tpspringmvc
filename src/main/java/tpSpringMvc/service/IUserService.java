package tpSpringMvc.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import tpSpringMvc.models.User;


public interface IUserService {
	public List<User> findAll ();
	public Page<User> findAll (Pageable p);
	public User findById (int id);
	public List<User> findByEmail (String e);
	public User findByLoginAndPass (String l, String p);
}
