package tpSpringMvc.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import tpSpringMvc.models.Event;


public interface IEventService {
	public List<Event> findAll ();
	public Page<Event> findAll (Pageable p);
	public Event findById (int id);
	public List<Event> findByTitle (String t);
	public Page<Event> findByTitle (String t, Pageable p);
}
