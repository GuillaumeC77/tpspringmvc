package tpSpringMvc.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import tpSpringMvc.models.Item;



public interface IItemService {
	public List<Item> findAll ();
	public Page<Item> findAll (Pageable p);
	public Item findById (int id);
	public List<Item> findByName (String n);
	public Page<Item> findByName (String n, Pageable p);
}
