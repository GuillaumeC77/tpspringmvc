package tpSpringMvc.config;

import java.util.Calendar;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import tpSpringMvc.models.Address;
import tpSpringMvc.models.Event;
import tpSpringMvc.models.Guest;
import tpSpringMvc.models.Item;
import tpSpringMvc.models.User;
import tpSpringMvc.service.AddressService;
import tpSpringMvc.service.EventService;
import tpSpringMvc.service.GuestService;
import tpSpringMvc.service.ItemService;
import tpSpringMvc.service.UserService;



public class App {

//	public App() {
//		// TODO Auto-generated constructor stub
//	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(AppConfig.class, JpaConfig.class);
		context.refresh();
		
		UserService userServ = context.getBean(UserService.class);
		AddressService addrServ = context.getBean(AddressService.class);
		EventService eventServ = context.getBean(EventService.class);
		GuestService guestServ = context.getBean(GuestService.class);
		ItemService itemServ = context.getBean(ItemService.class);
		
		
		userServ.createOrUpdateUser(new User(0, "root", "root", "mt@ajc.com", eventServ.findAll()));
		userServ.createOrUpdateUser(new User(0, "admin", "admin", "gc@ajc.com", null));
		userServ.createOrUpdateUser(new User(0, "user", "user", "ga@ajc.com", null));
		userServ.createOrUpdateUser(new User(0, "guest", "guest", "sa@ajc.com", null));
		
		
		addrServ.createOrUpdateAddress(new Address(0, "bureaux ajc 3e", "6, rue Rougemont", "3eme étage", "75009", "PARIS", eventServ.findById(1)));
		addrServ.createOrUpdateAddress(new Address(0, "bureaux ajc 2e", "6, rue Rougemont", "2eme étage", "75009", "PARIS", null));
		addrServ.createOrUpdateAddress(new Address(0, "bureaux ajc 4e", "6, rue Rougemont", "4eme étage", "75009", "PARIS", null));
		
		

		eventServ.createOrUpdateEvent(new Event(0, "training ekoura", "training animé par Godwin", Calendar.getInstance(), true, addrServ.findById(2), userServ.findByLoginAndPass("root", "root"), itemServ.findAll(), guestServ.findAll()));
		eventServ.createOrUpdateEvent(new Event(0, "tp ekoura", "tp de Godwin", Calendar.getInstance(), true, addrServ.findById(1), null, null, null));
		eventServ.createOrUpdateEvent(new Event(0, "projet kartina", "projet kartina", Calendar.getInstance(), true, addrServ.findById(4), null, null, null));

		
		
		guestServ.createOrUpdateGuest(new Guest(0, "toto", "toto@ekoura.com", eventServ.findAll()));
		guestServ.createOrUpdateGuest(new Guest(0, "titi", "titi@ekoura.com", eventServ.findAll()));
		guestServ.createOrUpdateGuest(new Guest(0, "tata", "tata@ekoura.com", null));
		
		
		itemServ.createOrUpdateItem(new Item(0, "MacBook", 12, 1, eventServ.findById(3)));
		itemServ.createOrUpdateItem(new Item(0, "Ecran", 12, 1, null));
		itemServ.createOrUpdateItem(new Item(0, "Magic mouse", 11, 2, null));
		
		addrServ.createOrUpdateAddress(new Address(0, "bureaux ajc 3e", "6, rue Rougemont", "3eme étage", "75009", "PARIS", eventServ.findById(2)));

		
		List<User> userList = userServ.findAll();
		System.out.println("count ==> " + userList.size());
		List<Address> addrList = addrServ.findAll();
		System.out.println("count ==> " + addrList.size());
		List<Event> evList = eventServ.findAll();
		System.out.println("count ==> " + evList.size());
		List<Guest> guestList = guestServ.findAll();
		System.out.println("count ==> " + guestList.size());
		List<Item> itemList = itemServ.findAll();
		System.out.println("count ==> " + itemList.size());
		
		
		
		
		for (Event event : evList) {
			System.out.println("ID = " + event.getId() + " title = " + event.getTitle());
		}
		
		context.close();
	}

}
