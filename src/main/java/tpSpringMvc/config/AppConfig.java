package tpSpringMvc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("tpSpringMvc")
public class AppConfig {

//	public AppConfig() {
//		// TODO Auto-generated constructor stub
//	}

}
