package tpSpringMvc.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;




@Configuration 
@EnableTransactionManagement
@EnableJpaRepositories("tpSpringMvc")		// Repository scann�
@PropertySource("classpath:database.properties")
public class JpaConfig {
	
	// Config JPA (persistence.xml=jpaConfig.java+database.properties

		@Autowired
        private Environment env;	
	
		@Bean(name="entityManagerFactory")
		public LocalContainerEntityManagerFactoryBean getEntityManagerFactoryBean() {
		    LocalContainerEntityManagerFactoryBean lcemfb = new LocalContainerEntityManagerFactoryBean();
		    lcemfb.setJpaVendorAdapter(getJpaVendorAdapter());
		    lcemfb.setDataSource(getDataSource());
		    lcemfb.setPersistenceUnitName("myJpaPersistenceUnit");
		    lcemfb.setPackagesToScan("tpSpringMvc.models");		// !important - Ici que l'on sp�cifie toutes les entit�s
		    lcemfb.setJpaProperties(jpaProperties());
		    return lcemfb;
		}
	
		
		@Bean
		public JpaVendorAdapter getJpaVendorAdapter() {
		    JpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		    return adapter;
		}
        
		@Bean
		public DataSource getDataSource() {
		    BasicDataSource dataSource = new BasicDataSource();
		    dataSource.setDriverClassName(this.env.getProperty("database.driverClassName"));
		    dataSource.setUrl(this.env.getProperty("database.url"));
		    dataSource.setUsername(this.env.getProperty("database.username"));
		    dataSource.setPassword(this.env.getProperty("database.password"));
		    return dataSource;
		}
		
		@Bean(name="transactionManager")
		public PlatformTransactionManager txManager(){
		    JpaTransactionManager jpaTransactionManager = new JpaTransactionManager(
				getEntityManagerFactoryBean().getObject());
		    return jpaTransactionManager;
		}
        
		private Properties jpaProperties() {
            Properties properties = new Properties();
            properties.put("hibernate.dialect", this.env.getProperty("hibernate.dialect"));
            properties.put("hibernate.show_sql", this.env.getProperty("hibernate.show_sql"));
            properties.put("hibernate.format_sql", this.env.getProperty("hibernate.format_sql"));
            properties.put("hibernate.id.new_generator_mappings", this.env.getProperty("hibernate.id.new_generator_mappings"));
            properties.put("hibernate.hbm2ddl.auto", this.env.getProperty("hibernate.hbm2ddl.auto"));
            return properties;        
        }	
}
