package tpSpringMvc.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name="guest")
public class Guest implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="guest_PK") 
	private int id;
	private String name;
	private String email;
	
	@ManyToMany
	@JoinTable(name="guestevent",
				joinColumns=@JoinColumn(name="event_PK"),
				inverseJoinColumns=@JoinColumn(name="guest_PK"))
	private List<Event> eventList;
	
	
	public Guest() {
		// TODO Auto-generated constructor stub
	}


//	public Guest(int id, String name, String email) {
//		super();
//		this.id = id;
//		this.name = name;
//		this.email = email;
//	}
	
	
	public Guest(int id, String name, String email, List<Event> eventList) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.eventList = eventList;
	}


	public int getId() {
		return this.id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return this.email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

}
