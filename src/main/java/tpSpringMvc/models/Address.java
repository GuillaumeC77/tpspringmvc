package tpSpringMvc.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;




@Entity
@Table(name="address")
public class Address implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="addr_PK") 	
	private int id;
	
	private String name;
	private String street;
	private String comments;
	private String zipCode;
	private String city;
	
	@OneToOne
    @JoinColumn(name = "event_PK")
	private Event event;
	
	
//	CTOR	
	public Address() {
		// TODO Auto-generated constructor stub
	}

//	public Address(int id, String name, String street, String comments, String zipCode, String city) {
//		super();
//		this.id = id;
//		this.name = name;
//		this.street = street;
//		this.comments = comments;
//		this.zipCode = zipCode;
//		this.city = city;
//	}

public Address(int id, String name, String street, String comments, String zipCode, String city, Event event) {
		super();
		this.id = id;
		this.name = name;
		this.street = street;
		this.comments = comments;
		this.zipCode = zipCode;
		this.city = city;
		this.event = event;
	}


	//	MUTATEURS
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
