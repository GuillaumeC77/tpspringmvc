package tpSpringMvc.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="item")
public class Item implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="item_PK") 	
	private int id;
	private String name;
	private int neededQuantity;
	private int currentQuantity;
	
	@ManyToOne
	@JoinColumn(name="event_PK")
	private Event event;
	
	
	public Item() {
		// TODO Auto-generated constructor stub
	}


//	public Item(int id, String name, int neededQuantity, int currentQuantity) {
//		super();
//		this.id = id;
//		this.name = name;
//		this.neededQuantity = neededQuantity;
//		this.currentQuantity = currentQuantity;
//	}
	

	public Item(int id, String name, int neededQuantity, int currentQuantity, Event event) {
		super();
		this.id = id;
		this.name = name;
		this.neededQuantity = neededQuantity;
		this.currentQuantity = currentQuantity;
		this.event = event;
	}

	

	public int getId() {
		return this.id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getNeededQuantity() {
		return this.neededQuantity;
	}


	public void setNeededQuantity(int neededQuantity) {
		this.neededQuantity = neededQuantity;
	}


	public int getCurrentQuantity() {
		return this.currentQuantity;
	}


	public void setCurrentQuantity(int currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

}
