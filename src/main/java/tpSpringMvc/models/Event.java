package tpSpringMvc.models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="event")
public class Event implements Serializable {

	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="event_PK") 
	private int id;
	private String title;
	private String description;
	
	@Temporal(TemporalType.DATE)
	private Calendar beginDate;
	private boolean allDay;
	
	@OneToOne(mappedBy ="event")
	private Address addr;
	
	@ManyToOne
	@JoinColumn(name="user_PK")
	private User user;
	
	@OneToMany(mappedBy="event")
	private List<Item> itemList;
	
//	@ManyToMany(mappedBy="eventColect")
	@ManyToMany
	@JoinTable(name="guestevent",
			joinColumns=@JoinColumn(name="event_PK"),
			inverseJoinColumns=@JoinColumn(name="guest_PK"))
	private List<Guest> guestList;
	
	
	
	public Event() {
		// TODO Auto-generated constructor stub
	}

//	public Event(int id, String title, String description, LocalDate beginDate, boolean allDay, Address address) {
//		super();
//		this.id = id;
//		this.title = title;
//		this.description = description;
//		this.beginDate = beginDate;
//		this.allDay = allDay;
//		this.addr = address;
//	}

	public Event(int id, String title, String description, Calendar beginDate, boolean allDay, Address addr, User user,
			List<Item> itemList, List<Guest> guestList) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.beginDate = beginDate;
		this.allDay = allDay;
		this.addr = addr;
		this.user = user;
		this.itemList = itemList;
		this.guestList = guestList;
	}

	public int getId() {
		return this.id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitle() {
		return this.title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Calendar getBeginDate() {
		return this.beginDate;
	}


	public void setBeginDate(Calendar beginDate) {
		this.beginDate = beginDate;
	}


	public boolean isAllDay() {
		return this.allDay;
	}


	public void setAllDay(boolean allDay) {
		this.allDay = allDay;
	}

}
