package tpSpringMvc.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="user")
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_PK") 
	private int id;
	private String login;
	private String pass;
	private String email;
	
	@OneToMany(mappedBy="user")
	private List<Event> eventList;
	
	
	public User() {
		// TODO Auto-generated constructor stub
	}


//	public User(int id, String login, String pass, String email) {
//		super();
//		this.id = id;
//		this.login = login;
//		this.pass = pass;
//		this.email = email;
//	}

	public User(int id, String login, String pass, String email, List<Event> eventList) {
		super();
		this.id = id;
		this.login = login;
		this.pass = pass;
		this.email = email;
		this.eventList = eventList;
	}


	public int getId() {
		return this.id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getLogin() {
		return this.login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPass() {
		return this.pass;
	}


	public void setPass(String pass) {
		this.pass = pass;
	}


	public String getEmail() {
		return this.email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

}
