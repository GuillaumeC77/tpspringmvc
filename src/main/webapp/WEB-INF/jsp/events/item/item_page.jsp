<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!-- http://localhost:8088/tpspringmvc/events/list -->
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
		<title>Item page</title>
	</head>
	<body>
		<h2>${relatedEvent.title}</h2>
		<spring:url value="/events/item/add" var="addURL" />
		<a href="${addURL}">Add Item</a>
		
		<h1>Item List</h1>
		<table width="100%" border="1">
			<tr>
				<th>Id</th>
				<th>Nom</th>
				<th>Quantité requise</th>
				<th>Quantité acquise</th>
				<th></th>
				<th></th>
			</tr>
			<c:forEach items="${listItem}" var="it">
				<tr>
					<td>${it.id}</td>
					<td>${it.nom}</td>
					<td>${it.neededQuantity}</td>
					<td>${it.currentQuantity}</td>
					<td>
						<spring:url value="/events/item/update/${item.id}" var="updateURL"><a href="${updateURL}">Editer</a></spring:url>
					</td>
					<td>
						<spring:url value="/events/item/delete/${item.id}" var="deleteURL"><a href="${deleteURL}">Supprimer</a></spring:url>
					</td>
				</tr>
			</c:forEach>
		</table>
		<a href="/events/list">Retour</a>
	</body>
</html>