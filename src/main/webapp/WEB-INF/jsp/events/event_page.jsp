<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!-- http://localhost:8088/tpspringmvc/events/list -->
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
		<title>Event page</title>
	</head>
	<body>
		<spring:url value="/events/add" var="addURL" />
		<a href="${addURL}">Add Event</a>
		
		<h1>Events List</h1>
		<table width="100%" border="1">
			<tr>
				<th>Id</th>
				<th>Titre</th>
				<th>Description</th>
				<th>Date de début</th>
				<th>Dure toute la journée</th>
				<th></th>
				<th></th>
			</tr>
			<c:forEach items="${listEvent}" var="event">
				<tr>
					<td>${event.id}</td>
					<td>${event.title}</td>
					<td>${event.description}</td>
					<td>${event.beginDate}</td>
					
					<td>
						<spring:url value="/events/update/${event.id}" var="updateURL"><a href="${updateURL}">Editer</a></spring:url>
					</td>
					<td>
						<spring:url value="/events/delete/${event.id}" var="deleteURL"><a href="${deleteURL}">Supprimer</a></spring:url>
					</td>
					<td>
						<spring:url value="/events/guest/list" var="guestsURL"><a href="${guestsURL}">Invités</a></spring:url>
					</td>
					<td>
						<spring:url value="/events/item/list" var="itemsURL"><a href="${itemsURL}">Liste objets</a></spring:url>
					</td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>