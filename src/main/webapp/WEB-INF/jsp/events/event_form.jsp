<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Insert title here</title>
	</head>
	<body>
		<h1>Employee Form</h1>
		<spring:url value="/event/save" var="saveURL"></spring:url>
		
		<form:form modelAttribute="eventForm" method="POST" action="${saveURL}">
			<form:hidden path="id" />
			<table>
				<tr>
					<td>Titre</td>
					<td><form:input path="title" /></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><form:input path="description" /></td>
				</tr>
				<tr>
					<td>Date de début</td>
					<td><form:input path="beginDate" /></td>
				</tr>
				<tr>
					<td>Dure tout la journée?</td>
					<td><form:checkbox path="allDay" /></td>
				</tr>
				<tr>
					<td>Adresse</td>
					<td>
					<c:if test="${!empty addrList}">
						<form:select path="address">
							<c:forEach items="${addrList}" var="addr">
								<form:option value="${addr}">${addr.name} ${addr.street} ${addr.zipCode} ${addr.city}</form:option>
							</c:forEach>
						</form:select>
					</c:if>
					<c:if test="${empty addrList}"></c:if>
					</td>
				</tr>
				<tr>
                	<td></td>
                	<td><button type="submit">Save</button></td>
            	</tr>
			</table>
		</form:form>
	</body>
</html>